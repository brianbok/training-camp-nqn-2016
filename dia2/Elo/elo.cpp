#include <iostream>
#include <math.h>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	
	string tournName;
	int	players;
	double mediaDouble;
	double playersDouble;
	cin>>tournName>>players;
	playersDouble = players;
	
	cout<<"Tournament: "<<tournName<<"\n";
	cout<<"Number of players: "<<players<<"\n";
	
	cout<<"New ratings:\n";
	
	forn(i, players){
		string lastName;
		int kInt, ra;
		double w,we;
		cin>>lastName>>ra>>w>>we>>kInt;
		
		mediaDouble += ra;
		
		double kDouble = (double) kInt;
		
		double rDouble = ra +  (kDouble * (w - we));
		int r = round(rDouble);
		
		
		cout<<lastName<<" "<<r<<"\n";
	}
	
	mediaDouble /= playersDouble;
	
	int mediaInt = round(mediaDouble);
	
	cout<<"Media Elo: "<<mediaInt<<"\n";
}

#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)
#define INF 10000000

using namespace std;

int calc(int p, int total, int res, vector<int>& used_boxes, const vector<vector<int> >& piles,const vector<vector<int> >& profit, const int m, const int a){
	if (p == a){
		return 0;
	}
	int resoriginal = res;
	int maxprofit = -INF;
	forn(i, min((int)piles[p].size(), m - total)){
		int newprof = profit[p][i] + calc(p+1, total+i, res, used_boxes,piles,profit, m,a);
		if (newprof > maxprofit){
			res = resoriginal + newprof;
			maxprofit = newprof;
			if (res > resoriginal){
				used_boxes[p] = i+1;
			}
		}
	}
	return res;
}

int main(){
	ios::sync_with_stdio(false);
	int a, m;
	cin>>a>>m;
	
	vector<vector<int> > piles(a);
	vector<vector<int> > profit(a);
	
	
	forn(i,a){
		int k;
		cin>>k;
		piles[i].resize(k+1);
		forsn(j,1,k+1){
			cin>>piles[i][j];
		}
	}
	
	forn(i,a){
		profit[i].resize(piles[i].size());
		profit[i][0] = 0;
		forsn(j,1,(int)piles[i].size()){
			profit[i][j] = (j>0 ? profit[i][j-1] : 0) + 10  - piles[i][j];
		}
	}
	
	vector<int>used_boxes(a,0);
	int cant_boxes = 0;
	int res = -INF;
	cout<<calc(0,0,res,used_boxes,piles,profit,m,a);
	forn(i,a){
		cant_boxes+= used_boxes[i];
	}
	cout<<" "<<cant_boxes<<"\n";
	forn(i,a){
		cout<<used_boxes[i]<<" ";
	}
	
}

#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int intlog(long long base, long long x) {
    return (int)(log(x) / log(base));
}

int main(){
	ios::sync_with_stdio(false);

	long long a,b,p;
	while(cin>>a){
		cin >> b;
		cin >> p;
		a--;
		
		long long answ = 0;
		long long res = p;
		
		for(long long i=0;i<=intlog(p,b);i++){
			answ-= (a/res);
			answ+= (b/res);
			res*=p;	
		}
	
		vector<int> digits;
		
		while(answ > 0){
			digits.push_back(answ % 8);
			answ/=8;	
		}
		
		
		for(int i=digits.size()-1;i>=0;i--){
			cout << digits[i];	
		}
		
		if(digits.size() == 0) cout << "0";
		
		cout << "\n";
			
	}
	
}

#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);

	string s1,s2;
	int n1,n2;
	vector<int> count_s1(256,0);
	vector<int> count_s2(256,0);
	
	cin>>n1>>n2;
	cin>>s1>>s2;
	forn(i,(int)s1.size()){
		count_s1[s1[i]]++;
	}
	
	forn(i,((int)s1.size())-1){
		count_s2[s2[i]]++;
	}
	
	int res = 0;
	
	forsn(i, ((int)s1.size())-1, (int)s2.size()){
		count_s2[s2[i]]++;
		if (i-(int)s1.size() >= 0){
			count_s2[s2[i-s1.size()]]--;
		}
		bool match = true;
		forn(j,256){
			if (count_s1[j] != count_s2[j] and (('a'<=j and j<='z') or ('A'<=j and j<='Z')) ){
				match = false;
			}
		}
		if (match){
			res++;
		}
	
	}
	
	
	cout<<res<<"\n";
}

#include <iostream>
#include <stdio.h>
#include <vector>
#include <math.h>
#include <iomanip>      // std::setprecision
#include <algorithm>      


#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

double abs (double x){

	return x > 0 ? x : -x;
}


struct Point {
	double x;
	double y;
	
	Point operator + (const Point& p2) const {
		Point res;
		res.x = x+p2.x;
		res.y = y+p2.y;
	
		return res;
	}
	
	Point operator * (double k) const{
		Point res;
		res.x = x * k;
		res.y = y * k;
		
		return res;
	}
	
	Point operator - (const Point& p2) const {
		return (*this + (p2 * (-1)));
	}
	
	double norma(){
		return sqrt(x*x + y*y);
	}
	
	double cruz_z(const Point& p2) const {
		return x*p2.y - y*p2.x;
	}
	/*
	istream& operator >> (istream& os)  {
		x.
		
		return os;
	} */
};

Point segment(Point p1, Point p2){
	return p2 - p1;
}

double round_1 (double x){
	return round(x * 10.0) / 10.0;
}

int main(){
	
	//cin.tie(NULL);
	
	int t;
	cin>>t;
	
	
	forn(casen,t){
		
		vector<double>lados(3);
		cin>>lados[0]>>lados[1]>>lados[2];
		
		sort(lados.begin(), lados.end());
		//cout<<lados[0]<<" "<<lados[1]<<" "<<lados[2]<<endl;
				
		Point a,b,c;
		a.x = 0.0;
		a.y = 0.0;
		
		b.x = lados[2];
		b.y = 0.0;
		
		double x = (lados[0]*lados[0] + lados[2]*lados[2] - lados[1]*lados[1]) / (2.0*lados[2]); 
		
		double y = sqrt(lados[0]*lados[0] - x*x);
		
		//cout<<x<<" "<<y<<endl;
		
		c.x = x;
		c.y = y;
		
		Point seg_ac = segment(a,c);
		
		Point r = a - seg_ac;
		
		Point p = segment(a,b) + b; 
		
		Point q = segment(b,c) + c;
		
		Point seg_rq = segment(r,q);
		Point seg_pq = segment(p,q);
		
		double res = abs(seg_rq.cruz_z(seg_pq)) / 2.0;
		
		printf("%.1f\n", res);
	
	}
}

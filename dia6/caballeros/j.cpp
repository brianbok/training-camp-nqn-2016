#include <iostream>
#include <vector>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

#define MAX 10000

int main(){
	
	
	vector<bool> isPrime(MAX,true);
	vector<int> primeDivisors(MAX,0);
	
	isPrime[0] = false;
	isPrime[1] = false;
	primeDivisors[1] = 1;
	
	for(int i=0;i<MAX;i++){
		if(isPrime[i]){
			for(int j=i;j<MAX;j+=i){
				isPrime[j] = false;
				primeDivisors[j]+=1;	
			}	
		}	
	}
	
	int answ[MAX];
	int curr = 0;
	for(int i=0;i<MAX;i++){
		if(primeDivisors[i] == 2) curr++;
		answ[i] = curr;	
	}
	int n;
	cin >> n;
	cout << answ[n] << "\n";
	
}

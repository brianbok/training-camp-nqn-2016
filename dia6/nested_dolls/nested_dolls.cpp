#include <iostream>
#include <vector>
#include <math.h>
#include <algorithm>
#include <map>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;



int main(){
	
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int t;
	cin >> t;
	
	for(int i=0;i<t;i++){
		int dollQty;
		cin >> dollQty;
		
		pair<int,int> dolls[dollQty];
		
		for(int j=0;j<dollQty;j++){
			cin >> dolls[j].first;
			cin >> dolls[j].second;	
		}
		
		sort(dolls,dolls+dollQty);
		
		reverse(dolls,dolls+dollQty);
		
		
		map<int,int> heights;
		int lastW = -1;
		int lastSeen = 0;
		
		int equalPointer = 0;
		
		
		for(int j=0;j<dollQty;j++){
			while(equalPointer < j && dolls[equalPointer].first > dolls[j].first){
				heights[dolls[equalPointer].second] += 1;
				equalPointer++;
			}
			
			//cout << dolls[j].first << " " << dolls[j].second << " ";
			int hei = dolls[j].second;
			//cout << hei << "  ";
	
			
			
			lastW = dolls[j].first;
			
			map<int,int>::iterator it = heights.lower_bound(hei+1);
			
			if(it == heights.end()){
				//heights[hei]= 1;
			}else{
				heights[it->first]-=1;
				if(heights[it->first] == 0) heights.erase(it);
				//heights[hei] += 1;	
			}
			
			
			
		}
		
		while(equalPointer < dollQty){
			heights[dolls[equalPointer].second] += 1;
			equalPointer++;
		}
		
		int answ = 0;
		
		for(map<int,int>::iterator it = heights.begin();it != heights.end();it++){
			answ+=it->second;
		}
		
		cout << answ << "\n";	
			
	}
	
}

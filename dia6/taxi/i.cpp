#include <iostream>
#include <vector>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

#define MAX 10000

int main(){
	
	
	int t;
	cin >> t;
	
	for(int i=0;i<t;i++){
		int n;
		cin >> n;
		int qty[5];
		for(int j=0;j<5;j++) qty[j]=0;
		for(int b=0;b<n;b++){
			int num;
			cin >> num;
			qty[num]+=1;	
		}
		int answ = qty[4];
		answ += qty[3];
		qty[1]-=qty[3];
		answ+= qty[2]/2;
		if(qty[2] % 2 == 1){
			answ+=1;
			if(qty[1] > 0) qty[1]-=2;	
		}
		if(qty[1] > 0){
			answ+=qty[1]/4;
			if(qty[1] % 4 != 0) answ ++;	
		}	
		cout << answ << "\n";
	}
	
}

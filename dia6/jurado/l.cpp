#include <iostream>
#include <vector>
#include <math.h>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){

	int t;
	cin >> t;
	
	int arr[t];
	bool valid[t];
	
	for(int i=0;i<t;i++){
		cin >> arr[i];
		valid[i] = 1;	
	}
	
	valid[t-1] = 0;
	
	for(int i = t-2;i>=0;i--){
		int abajo = t-i-1;
		if(arr[i] > abajo) valid[i] = 0;
		else{
			valid[i] = (arr[i] == abajo);
			if (i+arr[i] + 1 <= t-1){
				valid[i] |= valid[i+arr[i]+1];
			}
		}
	}
	
	for(int i=0;i<t;i++){
		if(valid[i]) cout << i << "\n";
	}
	
	cout << t << "\n";
	
}

#include <iostream>
#include <vector>
#include <stdio.h>
#include <math.h>
#include <iomanip>      // std::setprecision

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

long double abs (long double x){

	return x > 0 ? x : -x;
}


struct Point {
	long double x;
	long double y;
	
	Point operator + (const Point& p2) const {
		Point res;
		res.x = x+p2.x;
		res.y = y+p2.y;
	
		return res;
	}
	
	Point operator * (long double k) const{
		Point res;
		res.x = x * k;
		res.y = y * k;
		
		return res;
	}
	
	Point operator - (const Point& p2) const {
		return (*this + (p2 * (-1)));
	}
	
	long double norma(){
		return sqrt(x*x + y*y);
	}
	
	long double cruz_z(const Point& p2) const {
		return x*p2.y - y*p2.x;
	}
	/*
	istream& operator >> (istream& os)  {
		x.
		
		return os;
	} */
};

Point segment(Point p1, Point p2){
	return p2 - p1;
}

long double round_3 (long double x){
	return round(x * 1000.0) / 1000.0;
}

int main(){
	
	Point a,b,c,d,e,f;
	
	while(true){
		cin>>a.x>>a.y;
		cin>>b.x>>b.y;
		cin>>c.x>>c.y;
		cin>>d.x>>d.y;
		cin>>e.x>>e.y;
		cin>>f.x>>f.y;
		
		if (a.x == 0.0 && a.y == 0.0 && b.x == 0.0 && b.y == 0.0 && c.x == 0.0 && c.y== 0.0){
			if (d.x == 0.0 && d.y == 0.0 && e.x == 0.0 && e.y == 0.0 && f.x == 0.0 && f.y == 0.0){
				break;
			}
		}
		
		Point seg_de = segment(d,e);
		Point seg_fe = segment(f,e);
		Point seg_ac = segment(a,c);
		Point seg_ab = segment(a,b);
		
		long  double norma_ab = seg_ab.norma();
		long double norma_ac = seg_ac.norma();
		
		//long double sin_theta = (seg_ab.cruz_z(seg_ac)) / (seg_ab.norma() * seg_ac.norma()); 
		
		long double area = abs(seg_de.cruz_z(seg_fe)) / 2.0;
		//cout<<"area: "<<area<<endl;
		
		long double t = area / abs(seg_ab.cruz_z(seg_ac));
		
		long double norma_bg = t*norma_ac;
		//long double norma_bg = area / (segment(a,b).norma());
		
		
		//cout<<norma_bg<<endl;
		//cout<<"seg ac : "<<seg_ac.x<<" "<<seg_ac.y<<" "<<norma_ac<<endl;
		Point unitario_ac = seg_ac * (1.0/norma_ac);
		//cout<<"unitario_ac : "<<unitario_ac.x<<" "<<unitario_ac.y<<" "<<unitario_ac.norma()<<endl;
		
		
		Point h = a + unitario_ac * (norma_bg );
		
		Point g = h + segment(a,b);
		
		
		//cout<< round_3(g.x)<<" "<<round_3(g.y)<<" ";
		printf("%.3Lf %.3Lf", g.x, g.y);
		//cout<<round_3(h.x)<<" "<<round_3(h.y)<<"\n";
		printf(" %.3Lf %.3Lf\n", h.x, h.y);
	}
}

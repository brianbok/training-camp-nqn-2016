#include <iostream>
#include <vector>
#include <stack>
#include <set>

#define forn(i,n) for(int i=0;i<(n);i++)

#define forsn(i,s,n) for(int i=s;i<(n);i++)

#define MODU 10000007

#define MAXN 1000010

using namespace std;

typedef vector<set<int> > adjacency_list;
typedef long long int lli;



adjacency_list adj(MAXN);
adjacency_list transpose_adj(MAXN);
vector<lli> lucky(MAXN,0);
vector<int> components(MAXN, -1);
vector<bool> visited(MAXN, false);
stack<int> pseudo_toposort;
stack<int> toposort_copy;
int node_count = 0;

void count_lucky_ways(){
	while(!toposort_copy.empty()){
		int node = toposort_copy.top();
		toposort_copy.pop();
		
		for(auto neigh : adj[node]){
			if (components[neigh] != components[node]){
				lucky[neigh] += lucky[node];
				lucky[neigh] %= MODU;
			}
		}
	}
}

void visit(int node){
	
	if (!visited[node]){
		visited[node] = true;
		for( auto neigh: adj[node]){
			visit(neigh );
		}
		pseudo_toposort.push(node);
	}
}

void assign(int node, int component_id){
	if (components[node] == -1){
		components[node] = component_id;
		for (auto neigh: transpose_adj[node]){
			assign(neigh, component_id);
		}
	}
}

void calc_strongly_components(){
	int count_components = 0;
	
	for(int i=1;i<=node_count;i++){
		if (!visited[i]){
			visit(i);
		}
	}
	
	toposort_copy = pseudo_toposort;
	
	while(!pseudo_toposort.empty()){
		int node = pseudo_toposort.top();
		pseudo_toposort.pop();
		
		if (components[node] == -1){
			assign(node, count_components++);
		}
	}
	
	/*
	while(!pseudo_toposort.empty()){
		int node = pseudo_toposort.top();
		pseudo_toposort.pop();
		
		
		for(auto neigh : transpose_adj[node]){
			if (components[node] != -1){
				components[node] = components[neigh];
			}
		}
		
		
		if (components[node] == -1){
			components[node] = count_components++;
		}
	}*/
}

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int m,a,b;
	cin>>node_count>>m>>a>>b;	
	
	forn(i,m){
		int x,y;
		cin>>x>>y;
		
		adj[x].insert(y);
		transpose_adj[y].insert(x);
	}
	
	
	lucky[a] = 1ll;
	
	calc_strongly_components();
	/*
	forn(i,node_count){
		cout<<components[i]<<" ";
	}
	cout<<endl;
	*/
	count_lucky_ways();
	
	cout<<lucky[b]<<"\n";
}

#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cassert>
#include <queue>
#include <set>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	

	long long int n,m,r,e;
	while(cin >> n >> m >> r >> e){
	
		vector<int>vecinos[n+2];
		set<pair<int,int> > exists;
		bool visited[n+2];
		
		for(int i=0;i<n+2;i++) visited[i] = false;
		
		for(int i=0;i<m;i++){
			int a,b;
			cin >> a >> b;
			if(exists.find(make_pair(a,b)) == exists.end()){
				vecinos[a].push_back(b);
				exists.insert(make_pair(a,b));
			}
			if(exists.find(make_pair(b,a)) == exists.end()){
				vecinos[b].push_back(a);
				exists.insert(make_pair(b,a));
			}
		}
		
		long long int answ = 0;
		
		for(int i=1;i<=n;i++){
			if(visited[n]) continue;
			visited[i] = true;
			queue<int> q;
			q.push(i);
			long long int nodes = 0;
			long long int edges = 0;
			//cout << i << endl;
			while(!q.empty()){
				int node = q.front();
				//cout << node << " ";
				q.pop();
				nodes++;
				for(int j=0;j<(int)vecinos[node].size();j++){
					if(visited[vecinos[node][j]] == false){
						visited[vecinos[node][j]] = true;
						q.push(vecinos[node][j]);	
					}	
					edges++;
				}	
			}
			if(nodes == 1) continue;
			//cout << nodes << " "<< i << " " << edges << endl;
			long long r1 = nodes;
			r1*=(nodes-1);
			r1/=2;
			r1-=edges/2;
			r1*=r;
			long long r2 = nodes;
			r2*=e;
			answ+= min(r1,r2);	
		}
		
		cout << answ << "\n";
	}
	
}

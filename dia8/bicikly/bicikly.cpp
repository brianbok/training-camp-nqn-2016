#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cassert>
#include <queue>
#include <set>
#include <map>
#include <stack>
#include <stack>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

#define VISITED 2
#define TEMPORARY 1
#define NOT_VISITED 0

#define MAXN 10010

#define MODU 1000000000

using namespace std;	

typedef long long int lli;

bool visit(int n, const vector<vector<int> >& adj, stack<int>& toposort, vector<int>& visited, vector<int>& visitado_2){
	if (visited[n] == TEMPORARY and visitado_2[n] != NOT_VISITED){
		return false;
	}
	else{
		bool res = true;
		
		if (visited[n] == NOT_VISITED){
			visited[n] = TEMPORARY;
			for (vector<int>::const_iterator it = adj[n].begin();it!=adj[n].end();it++){
				if (!visit(*it, adj, toposort, visited, visitado_2)){
					res = false;
				}
			}
			visited[n] = VISITED;
			toposort.push(n);
		}
		
		return res;
	}
	
}

void marcar_llega_2(int n, const vector<vector<int> >& adj, vector<int>& visitado_2){
	if (visitado_2[n] == TEMPORARY){
		//nada
	}
	else{
		if (visitado_2[n] == NOT_VISITED){
			visitado_2[n] = TEMPORARY;
			for(vector<int>::const_iterator it =adj[n].begin();it!=adj[n].end();it++){
				marcar_llega_2(*it, adj, visitado_2);
			}
			visitado_2[n] = VISITED;
		}
	}
	
}

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);


	int n;
	int m;
	cin>>n>>m;

	vector<vector<int> > adj (n+5);

	vector<vector<int> > adj_inverso (n+5);

	forn(i,m){
		int x, y;
		cin>>x>>y;
		adj[x].push_back(y);
		adj_inverso[y].push_back(x);
	}
	
	stack<int> toposort;
	vector<int> visited(n+5, NOT_VISITED);
	vector<int> visitado_2(n+5, NOT_VISITED);
	
	marcar_llega_2(2, adj_inverso, visitado_2);
	int es_dag = visit(1, adj, toposort, visited, visitado_2);
	
	vector<lli > formas_llegar(n+5, 0);
	vector<bool> se_paso(n+5, false);
	formas_llegar[1] = 1ll;
	
	if (es_dag){
		while(!toposort.empty()){
			int n = toposort.top();
			toposort.pop();
			
			for(vector<int>::iterator it = adj[n].begin();it!=adj[n].end();it++){
				se_paso[*it] = se_paso[n];
				formas_llegar[*it] += formas_llegar[n] ;
				if (formas_llegar[*it] >= MODU){
					se_paso[*it] = true;
				} 
				formas_llegar[*it] %= MODU; 
			}
		}
		if (se_paso[2]){
			lli num = formas_llegar[2];
			int count_digits = 0;
			while(num ){
				count_digits++;
				num/= 10;
			}
			forn(i,9-count_digits){
				cout<<0;
			}
		}
		cout<<formas_llegar[2]<<"\n";
		
	}
	else{
		if (visited[2] == NOT_VISITED){
			cout<<0<<"\n";
		}
		else{
			cout<<"inf\n";
		}
	}
	
}

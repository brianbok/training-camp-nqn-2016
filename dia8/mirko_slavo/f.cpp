#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cassert>
#include <queue>
#include <set>
#include <map>
#include <string.h>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	
	int n;
	cin >> n;
	
	int qty1[121],qty11[121];
	int qty2[121],qty22[121];
	
	for(int i=0;i<101;i++){
		qty1[i]=0;
		qty2[i]=0;	
		qty11[i]=0;
		qty22[i]=0;
	}
	
	
	for(int i=0;i<n;i++){
		int a,b;
		cin >> a >> b;	
		qty1[a]++;
		qty2[b]++;
		
		int p1=1,p2=100;
		int maxSum = 0;
		
		memcpy(qty22,qty2,sizeof(int)*110);
		memcpy(qty11,qty1,sizeof(int)*110);
		
		
		while(p1 != 100 && p2 != 0){
			if(qty11[p1] == 0){
				p1++;
				continue;	
			}
			if(qty22[p2] == 0){
				p2--;
				continue;	
			}
			//cout << p1 << " " << p2 << " "<< qty1[p1] << " " << qty2[p2] << endl;
			int mn = min(qty11[p1], qty22[p2]);
			maxSum = max(maxSum, p1+p2);
			qty11[p1]-=mn;
			qty22[p2]-=mn;
		}
		
		cout << maxSum << "\n";
	}
	
	
}

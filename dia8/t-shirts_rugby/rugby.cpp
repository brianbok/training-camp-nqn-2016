#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cassert>
#include <queue>
#include <set>
#include <map>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

struct Trie {
	map<char, Trie> m;
	int usados[2];
	
	Trie(){
		usados[0] = 0;
		usados[1] = 0;
	}
};

void insert(Trie& tr, const string& palabra, int i, const int eq){
	tr.usados[eq]++;
	if (i != (int) palabra.size()){
		insert(tr.m[palabra[i]], palabra, i+1, eq);
	}
}

int count(Trie& tr, int i){
	int res = 0;
	
	if (i != 0){
		res += min(tr.usados[0], tr.usados[1]);
	}
	
	for(map<char,Trie>::iterator it = tr.m.begin();it!=tr.m.end();it++){
		res += count(it->second, i+1);
	}
	
	return res;

}

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	
	int n;
	while(cin>>n && n != -1){
		Trie root;
	
		forn(i,n){
			string s;
			cin>>s;
			insert(root, s, 0, 0);
		}
		
		forn(i,n){
			string s;
			cin>>s;
			insert(root,s,0,1);
		}
		
		cout<<count(root,0)<<"\n";
	}
}

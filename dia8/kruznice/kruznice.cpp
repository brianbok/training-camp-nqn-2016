#include <iostream>
#include <vector>
#include <algorithm>

#define forn(i,n) for(int i=0;i<(n);i++)

using namespace std;

template <class T>
struct Compressor {
	vector<T> uncompressed;
	
	T get(int idx){
		return uncompressed[idx];
	}
	
	int index(T coord){
		int low = 0;
		int high = uncompressed.size();
		
		while (high - low > 1){
			int mid = (low + high) / 2;
			
			if (uncompressed[mid] > coord){
				high = mid;
			}
			else{
				low = mid;
			}
		}
		return low;
	}
	
	Compressor(vector<T>coords){
		sort(coords.begin(),coords.end());
		
		forn(i,coords.size()){
			if (i == 0 or coords[i] != coords[i-1]){
				uncompressed.push_back(coords[i]);
			}
		}
		
	}
	
	int size(){
		return uncompressed.size();
	}
};

struct ComponentsDict{
	vector<vector<int> > components_list;
	vector<int > components;
	int count_components;
	
	ComponentsDict(int n){
		components.resize(n);
		components_list.resize(n);
		
		forn(i,n){
			components[i] = i;
			components_list[i].push_back(i);
		}
		
		count_components = n;
	}
	
	int find(int x){
		return components[x];
	}
	
	void join(int x, int y){
		const int comp_x = components[x];
		const int comp_y = components[y];
		
		if (comp_x != comp_y){
			count_components--;
		
			if (components_list[comp_x].size() > components_list[comp_y].size() ){
				for(auto node : components_list[comp_y]){
					components_list[comp_x].push_back(node);
					components[node] = comp_x;
				}
				components_list[comp_y].clear();
			}
			else{
				for(auto node: components_list[components[comp_x]]){
					components_list[comp_y].push_back(node);
					components[node] = comp_y;
				}
				
				components_list[comp_x].clear();
			}
			
		}
	}
	
	int size(){
		return count_components;
	}
};

int main(){
	int n;
	cin>>n;
	
	vector<int>coords(2*n);
	
	forn(i,n){
		int xi,ri;
		cin>>xi>>ri;
		
		coords[2*i] 		= xi-ri;
		coords[2*i + 1] 	= xi + ri;
	}
	
	Compressor<int> compressed_coords = Compressor<int>(coords);
	ComponentsDict components = ComponentsDict(compressed_coords.size());
	
	
	forn(i,n){
		int comp1 = compressed_coords.index(coords[2*i	]);
		int comp2 = compressed_coords.index(coords[2*i+1]);
		components.join(comp1, comp2);
	}
	
	int edges = 2*n;
	int vertices = compressed_coords.size();
	int connected_components = components.size();
	
	//euler's formula
	int regions = edges + connected_components + 1 - vertices;
	
	cout<<regions<<"\n";
}

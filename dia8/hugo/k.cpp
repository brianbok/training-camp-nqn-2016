#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cassert>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int arr[10];
int cache[10] = {1,10,100,1000,10000,100000,1000000,10000000,100000000,100000000};

int restar(int n){
	for(int i=0;i<10;i++){
		arr[i] = n % 10;
		n/=10;	
	}
	sort(arr,arr+10);
	for(int i=0;i<10;i++){
		n+=arr[i]*cache[10-i-1];	
	}
	return n;
}

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int num;
	cin >> num;
	
	int answ = 0;
	while(num > 0){
		int other = restar(num);
		num = num - other;
		answ++;	
	}
	
	cout << answ << "\n";
	
	
}

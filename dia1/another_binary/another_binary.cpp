#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <math.h>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

typedef long long int lli;


#define MAX 10e6+10

bool esPrimo(lli n){
	for(lli i = 2;i*i<=n;i++){
		if (n % i == 0){
			
			return false;
		}
	}
	
	return true;
}

int main(){
	
	//ios::sync_with_stdio(false);
	
	lli t;
	
	
	cin >> t;
	
	vector<bool>isPrime(MAX,true);
	
	vector<lli> primes;
	
	/*
	for(lli i=2;i<MAX;i++){
		if(isPrime[i]){
			for(int j=i;j<MAX;j+=i){
				isPrime[j] = false;
			}
			primes.push_back(i);
		}
	}
	
	*/
	forn(casen, t){
		lli divs_prims = 0;
		lli prime_actual = 2;
		lli num;
		lli res = 1;
		cin>>num;
		lli num_original = num;
		
		while(num>1 && prime_actual*prime_actual <= num_original){
			
			while(num % prime_actual == 0){
								
				divs_prims++;
				num /= prime_actual;
				if (divs_prims<3LL){
					res*= prime_actual;
				}
				
				
			}			
			prime_actual++;
		}
		if (num != 1){
			divs_prims++;
			if (divs_prims<3LL){
				res*= num;
			}
		}
		if (divs_prims == 2LL){
			cout<<"Molek\n";
		}
		else{
			cout<<"Vasha\n";
			if (divs_prims >= 3LL){
				cout<<res<<"\n";
			}
			if (divs_prims == 1LL){
				cout<<0<<"\n";
			}
		}
	}
	
	
}


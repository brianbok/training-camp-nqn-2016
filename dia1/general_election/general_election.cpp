#include <iostream>
#include <set>
#include <vector>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

typedef long long int lli;

int main(){
	ios::sync_with_stdio(false);
	
	
	int t;
	cin>>t;
	forn(casen, t){
		int n,m;
		cin>>n>>m;
		vector<lli>votes(n,0);
		forn(j,m){
			forn(i,n){
				lli num;
				cin>>num;
				votes[i] += num;
			}
		}
		lli  max_votes = -1;
		lli winner = 0;
		forn(i,n){
			if (votes[i] > max_votes){
				max_votes = votes[i];
				winner = i;
			}
		}
		cout<<winner+1<<endl;
	}	
}



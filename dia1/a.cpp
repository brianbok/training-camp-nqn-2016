#include <iostream>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	
	int n;
	cin >> n;
	
	forn(i,n){
		int e,f,c;
		cin >> e;
		cin >> f ;
		cin >> c;
		if(c < 2) cout << "0" << endl;
		else{
			int answ = 0;
			int bottles = e+f;
			while(bottles >= c ){
				int div = bottles / c;
				answ+=div;
				bottles = bottles / c + bottles % c;	
			}
			cout << answ  << endl;
		}	
	}
}

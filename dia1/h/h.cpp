#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
#include <math.h>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

#define MAX 1000007

vector<int> visited(MAX,-1)[6];

int pots[] = {1,10,100,1000,10000,100000};

vector<int> vecinos(int num, int dist, int n){
	vector<int> res;
	vector<int> digits(n,0);
	int original = num;
	
	forn(i,n){
		digits[i] = num % 10;
		num/=10;
	}
	
	forn(i,n){
		int temp = original - digits[i]*pots[i];
		
		int sol= temp + ((digits[i]+1)*(digits[i] != 9))*pots[i];
		
		if(sol < 0 or sol >= 1000000){}
		else{
		if(visited[sol] == -1){
			visited[sol] = dist+1;	
			res.push_back(sol);
		}
	}
		if(digits[i] == 0){
			sol = temp + 9 *pots[i];
		}else{
			sol = temp + ((digits[i]-1))*pots[i];
		}	
		
		if(sol < 0 or sol >= 1000000){}// cout << num << " gg " << sol<< endl;
		else{
			if(visited[sol] == -1){
				visited[sol] = dist+1;	
				res.push_back(sol);
			}
		}		
		
	}
	return res;
	
}

vector<bool>isPrime(MAX,true);
	
vector<int> primes;
queue<int> cola;
	

int main(){
	ios::sync_with_stdio(false);cin.tie(NULL);
	int n;
	cin >> n;
	
	
	for(int i=2;i<MAX;i++){
		if(isPrime[i]){
			for(int j=i;j<MAX;j+=i){
				isPrime[j] = false;
			}
			primes.push_back(i);
		}
	}

	for(int i=0;i<(int)primes.size();i++){
		cola.push(primes[i]);
		visited[primes[i]] = 0;
	}	
	
	while(cola.empty() == false){
		int prime = cola.front();
		cola.pop();
		
		vector<int> neighbours = vecinos(prime,visited[prime]);
		
		for(int i=0;i<(int)neighbours.size();i++){
			cola.push(neighbours[i]);	
		}		
	}
	
	
	for(int i=0;i<n;i++){
		int num, basofia;
		
		cin >> basofia>>num;
		
		cout<<visited[num]<<"\n";
	}				
	
}

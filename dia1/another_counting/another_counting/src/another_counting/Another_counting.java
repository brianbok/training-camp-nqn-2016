/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class Another_counting {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        PrintWriter pr = new PrintWriter(System.out);
        
        while(sc.hasNextBigInteger()){
            BigInteger n = sc.nextBigInteger();
            BigInteger c = sc.nextBigInteger();
            BigInteger m = sc.nextBigInteger();

            BigInteger res = c.modPow(n, m);

            pr.println(res);
        }
        pr.close();
    
    }
    
}

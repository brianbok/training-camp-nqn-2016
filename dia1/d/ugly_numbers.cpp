#include <iostream>
#include <set>
#include <vector>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

typedef long long int lli;

#define MAXN 1500

int main(){
	set<lli>nums;
	nums.insert(1);
	vector<lli>ugly(MAXN);
	
	forn(i,MAXN){
		lli num = *nums.begin();
		nums.erase(nums.begin());
		ugly[i] = num;
		nums.insert(num*2);
		nums.insert(num*3);
		nums.insert(num*5);
	}
	
	int n;
	
	while(cin>>n && n!=0){
		cout<<ugly[n-1]<<endl;
	}
}


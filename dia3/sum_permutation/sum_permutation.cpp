#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(lli i=0;i<(n);i++)
#define forsn(i,s,n)for(lli i=s;i<(n);i++)

#define MAXN 10e6 + 10

typedef long long int lli;

#define MODU	1000003

using namespace std;


int main(){
	ios::sync_with_stdio(false);
	
	vector<int>s(MAXN);
	s[1] = 1;
	
	forsn(i,2, MAXN){
		s[i] = (s[i-1] *  i  + 1) % MODU;
	}
	
	int t;
	cin>>t;
	
	forn(casen, t){
		int n;
		cin>>n;
		
		cout<<s[n]<<"\n";
	}
	
}

#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(lli i=0;i<(n);i++)
#define forsn(i,s,n)for(lli i=s;i<(n);i++)

#define MAXN (1000000ll + 10ll)

typedef long long int lli;

using namespace std;

vector<bool> esPrimo(MAXN+1, true);
vector<lli> primorial(MAXN+1,0);
	/*

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);

	esPrimo[0] = false;
	esPrimo[1] = false;
	esPrimo[2] = true;
	primorial[0] = 1;
	primorial[1] = 1;
	primorial[2] = 2;
	
	lli primor = 2;

	for(int i=3;i<MAXN;i+=2){
		if (esPrimo[i]){
			primorial[i] = primor*i;
			primorial[i] %= 10000000000LL;
			primor = primorial[i];
			lli j=i;
			j*=i;
			for(; j<MAXN;j+=i*2){
				esPrimo[j] = false;
			}			
		}
	}
	
	primor = primorial[3];
	
	for(int i=4;i<MAXN;i++){
		if(primorial[i] == 0) primorial[i] = primor;
		else primor = primorial[i];
	}
	
		
	int t;
	cin>>t;
	
	for(int i=0;i<t;i++){
		int n;
		cin>>n;
		
		cout<<primorial[n]<<"\n";
	}
	
}*/


int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	esPrimo[0] = false;
	esPrimo[1] = false;
	esPrimo[2] = true;
	primorial[0] = 1;
	primorial[1] = 1;
	
	/*
	for(int j = 4;j<MAXN;j+=2){
		esPrimo[j] = false;
	}*/
	
	vector<bool> sepaso(MAXN, false);
	
	forsn(i,2ll,MAXN){
		primorial[i] = primorial[i-1];
		sepaso[i] = sepaso[i-1];
		if (esPrimo[i]){
			primorial[i] *= i;
			
			//modulo 10 digitos
			if (primorial[i] >=10000000000LL ){
				sepaso[i] = true;
			}
			primorial[i] %= 10000000000LL;
			//if (i>2ll){
				for(lli j=i; j<MAXN;j+=i){
					esPrimo[j] = false;
				}
			//}
			
		}
	}
	
	int t;
	cin>>t;
	
	forn(casen, t){
		lli n;
		cin>>n;
		if (sepaso[n]){
			lli count = 0;
			lli num = primorial[n];
			
			while(num > 0){
				//cout<<num<<endl;
				num/= 10;
				count++;
			}
			forn(i,10-count){
				cout<<0;
			}
		}
		cout<<primorial[n]<<"\n";
	}
	
}


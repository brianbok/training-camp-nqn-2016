#include <iostream>
#include <algorithm>
#include <math.h>
#include <set>
#include <vector>
#define forn(i,n)	for(lli i=0;i<(n);i++)
#define forsn(i,s,n)for(lli i=s;i<(n);i++)

#define MAXN 10e6 + 10

typedef long long int lli;

#define MODU	1000003

using namespace std;

void calc(lli i, lli num, vector<lli>& sols, const vector<lli>& arr, const lli n, const lli r1, const lli r2){
	if (i == n){
		if (num>= r1 && num<= r2){
			sols.push_back(num);
		}
	}
	else{
		//cout<<i<<" "<<endl;
		for(lli p=1;p*num <= r2; p*= arr[i]){
			calc(i+1, p*num, sols, arr, n, r1, r2);
		}
	}
}


int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	lli n;
	while(cin >> n && n != 0){
		vector<lli> arr(n);
		for(lli i=0;i<n;i++){
			cin >> arr[i];
		}
		
		lli r1,r2;
		cin >> r1 >> r2;
		
		vector<lli> sols;
		
		calc(0,1,sols,arr, n, r1,r2);
		
		sort(sols.begin(),sols.end());
		
		for(lli i=0;i<sols.size();i++){
			if(i+1 == sols.size()){
				cout << sols[i];	
			}else{
				cout << sols[i] << ",";	
			}
		}
		
		if(sols.size() == 0) cout << "none";
		
		cout << "\n";
			
	}
	
}

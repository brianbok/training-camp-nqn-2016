#include <iostream>
#include <math.h>
#include <set>
#include <vector>
#define forn(i,n)	for(lli i=0;i<(n);i++)
#define forsn(i,s,n)for(lli i=s;i<(n);i++)

#define MAXN 10e6 + 10

typedef long long int lli;

#define MODU	1000003

using namespace std;


int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int n;
	while(cin >> n){
		if(n == 0) break;
		lli arr[n];
		set<lli> s;
		for(int i=0;i<n;i++){
			cin >> arr[i];
			s.insert(arr[i]);	
		}
		lli r1,r2;
		cin >> r1 >> r2;
		
		vector<lli> sols;
		
		if(r1 == 1) sols.push_back(1);
		
		while(s.empty() == false){
			lli num = *s.begin();
			s.erase(s.begin());
			if(num > r2) break;
			if(num >= r1) sols.push_back(num);
			for(int i=0;i<n;i++){
				if(num*arr[i] > 0){
					s.insert(num*arr[i]);
				}	
			}
		}
		
		for(int i=0;i<sols.size();i++){
			if(i+1 == sols.size()){
				cout << sols[i];	
			}else{
				cout << sols[i] << ",";	
			}
		}
		
		if(sols.size() == 0) cout << "none";
		
		cout << "\n";
			
	}
	
}

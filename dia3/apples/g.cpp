#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;


int main(){
	ios::sync_with_stdio(false);
	
	int n;
	
	while(cin >> n){
		int impares = 0;
		int pares = 0;
		int suma = 0;
		
		for(int i=0;i<n;i++){
			int bg;
			cin >> bg;
			suma = suma + bg % 2;
			if(bg % 2 == 1) impares++;
			else pares++; 
		}
		if(suma % 2 == 1) cout << impares;
		else cout << pares;
		cout << "\n";
		
	}
	
}

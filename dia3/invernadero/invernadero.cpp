#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(lli i=0;i<(n);i++)
#define forsn(i,s,n)for(lli i=s;i<(n);i++)

#define MAXN 10e6 + 10

typedef long long int lli;

#define MODU	1000003

using namespace std;


int main(){
	ios::sync_with_stdio(false);
	
	int x,y, p,f;
	int res = -1, res_x,res_y,res_lado;
	
	cin>>x>>y;
	cin>>p>>f;
	
	char mapa[x+5][y+5];
	int suma_p[x+5][y+5];
	int suma_f[x+5][y+5];
	
	
	forn(i,x+5){
		forn(j,y+5){
			suma_f[i][j] = 0;
			suma_p[i][j] = 0;
			
			mapa[i][j] = 0;
		}
	}
	
	forn(i,p){
		int posx, posy;
		cin>>posx>>posy;
		mapa[posx-1][posy-1] = 'p';
	}
	
	forn(i,f){
		int posx, posy;
		cin>>posx>>posy;
		mapa[posx-1][posy-1] = 'f';
	}
	
	forsn(i,1,x+2){
		forsn(j,1,y+2){
			suma_f[i][j] = suma_f[i-1][j] + suma_f[i][j-1] - suma_f[i-1][j-1];
			suma_p[i][j] = suma_p[i-1][j] + suma_p[i][j-1] - suma_p[i-1][j-1];
			if (mapa[i-1][j-1] == 'f'){
				suma_f[i][j]++;
			}
			if (mapa[i-1][j-1] == 'p'){
				suma_p[i][j]++;
			}
			//cout << suma_p[i][j] << " ";
		}//cout << endl;
	}
	
	
	
	forn(i,x){
		forn(j,y){
			
			int lowP = 0;
			int highP = min(x-i+1,y-j+1) + 1;
			
			while(highP - lowP > 1){
				int mid = (lowP+highP) / 2;
				
				if (suma_p[i+mid][j+mid] + suma_p[i][j] - suma_p[i][j+mid] - suma_p[i+mid][j] > 0){
					highP = mid;
				}
				else{
					lowP = mid;
				}
			}
			
			int cant_f = suma_f[i+lowP][j+lowP] + suma_f[i][j] - suma_f[i][j+lowP] - suma_f[i+lowP][j];
			
			int lowF = 0;
			int	highF = lowP+1;
			while(highF - lowF > 1){
				int mid = (lowF+highF) / 2;
				
				if (suma_f[i+mid][j+mid] + suma_f[i][j] - suma_f[i][j+mid] - suma_f[i+mid][j] < cant_f){
					lowF = mid;
				}
				else{
					highF = mid;
				}
			}
			
			if (cant_f > res ){
				res = cant_f;
				res_x = i+1;
				res_y = j+1;
				
				res_lado = highF;
			
			}
			
			
		}
	}
	
	cout<<res_x<<" "<<res_y<<"\n";
	cout<<res_lado<<"\n";
	cout<<res<<"\n";
	
}

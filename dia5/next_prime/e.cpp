#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

#define MAX 70000

vector<bool> isPrime(MAX,true);
vector<int> primes;

bool is_prime(long long int n){
	if(n < MAX) return isPrime[n];
	for(int i=0;i<primes.size();i++){
		if(n % primes[i] == 0) return false;	
	}
	return true;	
}

int main(){
	ios::sync_with_stdio(false);
	
	isPrime[0] = false;
	isPrime[1] = false;
	
	primes.push_back(2);
	
	for(unsigned long long i=4;i<MAX;i+=2) isPrime[i] = false;
	
	for(unsigned long long i=3;i<MAX;i+=2){
		if(isPrime[i]){
			primes.push_back(i);
			for(unsigned long long j=i*i;j<MAX;j+=i*2) isPrime[j] = false;	
		}	
	}
	 int t;
	cin >> t;
	
	for(int i=0;i<t;i++){
		long long int num;
		cin >> num;
		while(is_prime(num) == false){
			num++;	
		}
		cout << num << endl;
	}
	
	
	
}

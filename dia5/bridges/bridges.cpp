#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <cassert>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

#define MAXY 60005

vector<int> fenwick(MAXY+1);

typedef long long int lli;

int fenwick_accum(vector<int>& fen, int i){
	int res = 0;
	while (i != 0){
		res += fen[i];
		i = (i-1)&i;
	}
	return res;
}

void fenwick_add(vector<int>& fen, int i, int x){
	int ceros = ~i;
	for(int bit = ceros & (-ceros); ; ceros -= bit, bit = ceros & (-ceros)){
		int index = (i & ~(bit-1)) | bit;
		
		if (index > (int)fen.size())
			break;
			
		fen[index] += x;
	}  
}

struct evento{
	bool esVertical;
	int posx;
	int numeroTramo;
	bool esInicio;
	
	bool operator < (const evento& e2) const {
		if (this->posx != e2.posx){
			return posx < e2.posx;
		}
		else{
			if (esVertical && !e2.esVertical ){
				return true;
			}
			if (!esVertical and !e2.esVertical ){
				return false;
			} // vertical < no vertical
			
			if (esInicio && ! e2.esInicio){
				return true;
			}
			
			return numeroTramo < e2.numeroTramo;
			
			
		}
	}
	evento(){
		numeroTramo = 31000;
	}
};


int main(){
	ios::sync_with_stdio(false);cin.tie(NULL);
	

	
	int t1,t2;
	lli res = 0;
	cin >> t1;
	
	vector<int>posx_tramo;
	vector<int>posy_tramo;
	
	vector<evento> eventos;
	
	forn(i,t1){
		int x,y;
		
		cin>>x>>y;
		
		posx_tramo.push_back(x);
		posy_tramo.push_back(y);
		
		if (i>0){
			if (posy_tramo[i] == posy_tramo[i-1]){
				evento eInicio;
				eInicio.esVertical = false;
				eInicio.esInicio = true;
				eInicio.numeroTramo = i-1;
				eInicio.posx = posx_tramo[i-1];
				
				eventos.push_back(eInicio);
				assert(eventos[eventos.size()-1].numeroTramo == i-1 );
				
				evento eFinal;
				eFinal.esVertical = false;
				eFinal.esInicio = false;
				eFinal.numeroTramo = i;
				eFinal.posx = posx_tramo[i-1];
				
				eventos.push_back(eFinal);
				assert(eventos[eventos.size()-1].numeroTramo == i );
				
			}
			else{
				evento eVertical;
				eVertical.esVertical = true;
				eVertical.esInicio = false;
				eVertical.numeroTramo = i;
				eVertical.posx = posx_tramo[i];
				
				eventos.push_back(eVertical);
				
			}
		}
		
	}
	cin>>t2;
	
	forsn(i,t1,t2){
		int x,y;
		
		cin>>x>>y;
		
		posx_tramo.push_back(x);
		posy_tramo.push_back(y);
		
		if (i>t1){
			if (posy_tramo[i] == posy_tramo[i-1]){
				evento eInicio;
				eInicio.esVertical = false;
				eInicio.esInicio = true;
				eInicio.numeroTramo = i-1;
				eInicio.posx = posx_tramo[i-1];
				
				eventos.push_back(eInicio);
				
				evento eFinal;
				eFinal.esVertical = false;
				eFinal.esInicio = false;
				eFinal.numeroTramo = i;
				eFinal.posx = posx_tramo[i-1];
				
				eventos.push_back(eFinal);
			}
			else{
				evento eVertical;
				eVertical.esVertical = true;
				eVertical.esInicio = false;
				eVertical.numeroTramo = i;
				eVertical.posx = posx_tramo[i];
				
				eventos.push_back(eVertical);
			}
			
		}
				
	}
	
	sort(eventos.begin(), eventos.end());
	
	for(vector<evento>::iterator it = eventos.begin();it!=eventos.end();it++){
		//cout<<"ba\n"<<flush;
		cout<<"posx: "<<it->posx<<endl;
		if (! it->esVertical){
			//cout<<it->numeroTramo<<endl;
			cout<<posy_tramo[it->numeroTramo]<<endl;
			if (! it->esInicio){
				fenwick_add(fenwick, posy_tramo[it->numeroTramo], -1 ); // sacalo
			} 
			else{
				fenwick_add(fenwick, posy_tramo[it->numeroTramo], 1 ); // ponelo
			}
		}
		else {
			
			int pos1 = posy_tramo[it->numeroTramo];
			int pos2 = posy_tramo[it->numeroTramo-1];
			int posy_max = max(pos1,pos2);
			int posy_min = min(pos1,pos2);
			
			int maxCount = fenwick_accum(fenwick,posy_max);
			int minCount = fenwick_accum(fenwick,posy_min+1);
			
			//cout<<maxCount<<" "<<minCount<<" "<<endl;
			
			res += maxCount - minCount; 
		}
	}
	
	cout<<res<<endl;
	
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Scanner;

/**
 *
 * @author alumno
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(System.out));
        Scanner sc = new Scanner(br);
        PrintWriter pr = new PrintWriter(bw);
        
        int m = sc.nextInt();
        
        for(int i = 0;i< m;i++){
            int n = sc.nextInt();
            
            BigInteger dos = BigInteger.valueOf(2);
            pr.println(dos.pow(n));
            
        }
        
    }
    
}

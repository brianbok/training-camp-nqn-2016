#include <iostream>
#include <math.h>
#include <vector>
#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

#define MAX 10e6+10

vector<bool> isPrime(MAX,true);
vector<int> primes;

typedef long long int lli;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	
	isPrime[0] = false;
	isPrime[1] = false;
	
	primes.push_back(2);
	
	for(unsigned long long i=4;i<MAX;i+=2) isPrime[i] = false;
	
	for(unsigned long long i=3;i<MAX;i+=2){
		if(isPrime[i]){
			primes.push_back(i);
			for(unsigned long long j=i*i;j<MAX;j+=i*2) isPrime[j] = false;	
		}	
	}
	

	lli n;
	while(cin>>n && n != 0){
		
		for(lli i = 0; i< primes.size();i++){
		
			lli p = primes[i];
			if (isPrime[n-p]){
				cout <<n<<" = "<<p<<" + "<<n-p<<"\n";
				break;
			}	
	
		}
		
	}
	
	
	
}

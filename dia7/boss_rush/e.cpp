#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <map>

#include <cassert>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int t;
	cin >> t;
	
	for(int i=0;i<t;i++){
		int n;
		cin >> n;
		pair<int,int> monsters[n*2];
		for(int j=0;j<n;j++){
			int normal,hard;
			cin >> normal >> hard;
			monsters[j]= make_pair(normal,1);
			if(normal > hard){
				hard = normal+1;	
			}
			monsters[j+n] = make_pair(hard,2);	
		}	
		
		sort(monsters,monsters+2*n);
		
		int answ = 0;
		int stars = 0;
		for(int j=0;j<2*n;j++){
			//cout << answ << " " << i << " ";
			if(monsters[j].first > stars){
				answ+=monsters[j].first-stars;
				stars = monsters[j].first;	
			}
			stars+=monsters[j].second;
		}
		cout << answ << "\n";
	}
	
	
}

#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <map>
#include <queue>
#include <set>

#include <cassert>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int n;
	cin >> n;
	
	int qty1[300];
	int qty2[300];
	
	for(int i=0;i<300;i++){
		qty1[i]=0;
		qty2[i]=0;	
	}
	
	char pila1[n];
	char pila2[n];
	
	int pos1 = 0;
	int pos2 = 0;
	
	int answ = 0;
	
	set<char> s1;
	set<char> s2;
	
	for(int i=0;i<n;i++){
		char food;
		cin >> food;
		//cout << i << " " << food << " " << qty1[food] << " " << qty2[food] << " " << answ << endl;
		if(qty1[food] < qty2[food] or qty1[food] == qty2[food] && pos1 > pos2){
			pila1[pos1] = food;
			qty1[food]+=1;
			pos1++;
			s1.insert(food);
			if(pos1 > 3){
				qty1[pila1[pos1-4]]-=1;
				if(qty1[pila1[pos1-4]] == 0) s1.erase(pila1[pos1-4]);	
			}	
			answ+=s1.size();
		}else{
			pila2[pos2] = food;
			qty2[food]+=1;
			pos2++;
			s2.insert(food);
			if(pos2 > 3){
				qty2[pila2[pos2-4]]-=1;
				if(qty2[pila2[pos2-4]] == 0) s2.erase(pila2[pos2-4]);	
			}
			answ+=s2.size();
		}
			
	}
	
	/*for(int i=0;i<pos1;i++){
		cout <<	pila1[i] << " ";
	}
	
	cout << endl;
	for(int i=0;i<pos2;i++){
		cout <<	pila2[i] << " ";
	}*/	
	
	cout << answ << "\n";
	
	
}

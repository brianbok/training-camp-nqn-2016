#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <map>

#include <cassert>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int l;
	int n;
	cin >> l>>n;
	
	vector<int>a(n);
	
	forn(i,n){
		cin>>a[i];
	}
	
	int i = 0;
	while(a[i] == 0){
		i++;
	}
	
	vector<int > posibilidades;
	
	int j = i;
	int suma = 0;
	int res = 0;
	
	while(j != n){
		while(j != n and suma + a[j] <= l){
			suma += a[j];
			j++;
		}
		
		if (suma!=0){
			res+= n-i;
			posibilidades.push_back(i+1);
		}
		
		suma = 0;
		i = j;
	
	}
	
	cout<<res<<"\n";
	for(vector<int>::iterator it = posibilidades.begin();it!=posibilidades.end();it++){
			if (it != posibilidades.begin()){
				cout<<" ";
			}
			cout<<*it;
	}
	cout<<"\n";
}

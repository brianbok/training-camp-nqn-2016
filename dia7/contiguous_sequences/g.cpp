#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <map>

#include <cassert>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	int t;
	cin >> t;
	
	for(int i=0;i<t;i++){
		int zeros,ones;
		cin >> zeros >> ones;
		long long int answ1 = 0;
		long long int answ2 = 0;
				
		answ1 = zeros*zeros - (ones/2+ones%2)*(ones/2+ones%2) - (ones/2)*(ones/2);	
	
		int groups = zeros+1;
		int qty = ones/groups;
		answ2 = zeros;
		answ2 -= (qty+1)*(qty+1)*(ones%groups);
		answ2 -= (qty)*(qty)*(groups-(ones%groups));
		
		cout << max(answ1,answ2) << "\n";		
	}
	
}

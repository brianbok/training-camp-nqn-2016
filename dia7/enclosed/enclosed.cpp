#include <iostream>
#include <math.h>
#include <vector>
#include <algorithm>
#include <map>

#include <cassert>

#define forn(i,n)	for(int i=0;i<(n);i++)
#define forsn(i,s,n)for(int i=s;i<(n);i++)

using namespace std;

int main(){
	ios::sync_with_stdio(false);
	cin.tie(NULL);
	
	map<char, int>m ;
	m['A'] = 1;
	m['B'] = 2;
	m['D'] = 1;
	m['O'] = 1;
	m['P'] = 1;
	m['Q'] = 1;
	m['R'] = 1;
	
	int res = 0;
	string s;
	cin>>s;
	forn(i,(int) s.size()){
		res += m[s[i]];
		
	}
	
	cout<<res<<"\n";
}
